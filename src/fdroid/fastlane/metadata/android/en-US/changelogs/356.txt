Fixed:
- Crash due to a library update
- Missing animate profile pictures
- Fix some color issues