Added:
- Offset for replies in threads with a new decoration to make them easier to read
- Fix translation issue (use https://translate.fedilab.app)
- Watermarks for pictures (default: disabled)
- Blur sensitive media for Pixelfed
- Put messages into drafts when replying to a deleted message
- Allow to schedule with another account

Fixed:
- Quick replies lose text when doing another action
- Fix refresh token (Peertube/Pixelfed)