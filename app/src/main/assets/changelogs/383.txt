/!\ PIXELFED ACCOUNTS MUST BE LOGOUT/LOGIN AGAIN
- Use a new API for Pixelfed (fix 2FA issue)
- Push notifications with UnifiedPush (More at https://fedilab.app/wiki/features/push-notifications)
- Support Scribe.rip and wikiless
- Delayed and live notifications removed
- Remove lost space for Pleroma accounts
- Fix a crash with console mode
- Fix an issue with Pleroma accounts when visiting a profile