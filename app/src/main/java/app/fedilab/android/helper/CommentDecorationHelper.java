package app.fedilab.android.helper;
/* Copyright 2021 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */


import org.jetbrains.annotations.NotNull;

import java.util.List;

import app.fedilab.android.client.Entities.Status;

public class CommentDecorationHelper {

    public static int getIndentation(@NotNull String replyToCommentId, List<Status> statuses) {
        return numberOfIndentation(0, replyToCommentId, statuses);
    }


    private static int numberOfIndentation(int currentIdentation, String replyToCommentId, List<Status> statuses) {

        String targetedComment = null;
        for (Status status : statuses) {
            if (replyToCommentId.compareTo(status.getId()) == 0) {
                targetedComment = status.getIn_reply_to_id();
                break;
            }
        }
        if (targetedComment != null) {
            currentIdentation++;
            return numberOfIndentation(currentIdentation, targetedComment, statuses);
        } else {
            return Math.min(currentIdentation, 15);
        }
    }

}
