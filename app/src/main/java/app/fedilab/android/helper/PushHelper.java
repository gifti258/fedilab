package app.fedilab.android.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import org.unifiedpush.android.connector.Registration;

import java.util.List;
import java.util.Set;

import app.fedilab.android.R;
import app.fedilab.android.client.Entities.Account;
import app.fedilab.android.jobs.ApplicationJob;
import app.fedilab.android.jobs.NotificationsSyncJob;
import app.fedilab.android.sqlite.AccountDAO;
import app.fedilab.android.sqlite.Sqlite;

import static android.content.Context.MODE_PRIVATE;
import static app.fedilab.android.helper.BaseHelper.NOTIF_NONE;
import static app.fedilab.android.helper.BaseHelper.NOTIF_PUSH;
import static app.fedilab.android.helper.BaseHelper.liveNotifType;
import static app.fedilab.android.jobs.BaseNotificationsSyncJob.NOTIFICATION_REFRESH;

import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;

public class PushHelper {

    public static void startStreaming(Context context) {
        int liveNotifications = liveNotifType(context);
        ApplicationJob.cancelAllJob(NOTIFICATION_REFRESH);
        NotificationsSyncJob.schedule(false);
        switch (liveNotifications) {
            case NOTIF_PUSH:
                new Thread(() -> {
                    SQLiteDatabase db = Sqlite.getInstance(context.getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
                    List<Account> accounts = new AccountDAO(context, db).getPushNotificationAccounts();
                    ((Activity) context).runOnUiThread(() -> {
                        Registration registration = new Registration();
                        List<String> distributors = registration.getDistributors(context);
                        if (distributors.size() == 0) {
                            SharedPreferences sharedpreferences = context.getSharedPreferences(Helper.APP_PREFS, MODE_PRIVATE);
                            int theme = sharedpreferences.getInt(Helper.SET_THEME, Helper.THEME_DARK);
                            int style;
                            if (theme == Helper.THEME_DARK) {
                                style = R.style.DialogDark;
                            } else if (theme == Helper.THEME_BLACK) {
                                style = R.style.DialogBlack;
                            } else {
                                style = R.style.Dialog;
                            }
                            AlertDialog.Builder alert = new AlertDialog.Builder(context, style);
                            alert.setTitle(R.string.no_distributors_found);
                            final TextView message = new TextView(context);
                            String link = "https://fedilab.app/wiki/features/push-notifications/";
                            final SpannableString s =
                                    new SpannableString(context.getString(R.string.no_distributors_explanation, link));
                            Linkify.addLinks(s, Linkify.WEB_URLS);
                            message.setText(s);
                            message.setPadding(30, 20, 30, 10);
                            message.setMovementMethod(LinkMovementMethod.getInstance());
                            alert.setView(message);
                            alert.setPositiveButton(R.string.close, (dialog, whichButton) -> dialog.dismiss());
                            alert.show();
                        } else {
                            registerAppWithDialog(context, accounts);
                        }
                    });
                }).start();
                //Cancel scheduled jobs
                Set<JobRequest> jobRequests = JobManager.instance().getAllJobRequestsForTag(NOTIFICATION_REFRESH);
                if (jobRequests != null && jobRequests.size() > 0) {
                    for (JobRequest jobRequest : jobRequests) {
                        JobManager.instance().cancel(jobRequest.getJobId());
                    }
                }
                break;
            case NOTIF_NONE:
                new Thread(() -> {
                    SQLiteDatabase db = Sqlite.getInstance(context.getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
                    List<Account> accounts = new AccountDAO(context, db).getPushNotificationAccounts();
                    ((Activity) context).runOnUiThread(() -> {
                        for (Account account : accounts) {
                            new Registration().unregisterApp(context, account.getUsername() + "@" + account.getInstance());
                        }
                    });
                }).start();
                NotificationsSyncJob.schedule(false);
                break;
        }
    }


    private static void registerAppWithDialog(Context context, List<Account> accounts) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(Helper.APP_PREFS, MODE_PRIVATE);
        int theme = sharedpreferences.getInt(Helper.SET_THEME, Helper.THEME_DARK);
        int style;
        if (theme == Helper.THEME_DARK) {
            style = R.style.DialogDark;
        } else if (theme == Helper.THEME_BLACK) {
            style = R.style.DialogBlack;
        } else {
            style = R.style.Dialog;
        }

        Registration registration = new Registration();
        List<String> distributors = registration.getDistributors(context);
        if (distributors.size() == 1 || !registration.getDistributor(context).isEmpty()) {
            if (distributors.size() == 1) {
                registration.saveDistributor(context, distributors.get(0));
            }
            for (Account account : accounts) {
                registration.registerApp(context, account.getUsername() + "@" + account.getInstance());
            }
            return;
        }

        AlertDialog.Builder alert = new AlertDialog.Builder(context, style);
        alert.setTitle(R.string.select_distributors);
        String[] distributorsStr = distributors.toArray(new String[0]);
        alert.setSingleChoiceItems(distributorsStr, -1, (dialog, item) -> {
            String distributor = distributorsStr[item];
            registration.saveDistributor(context, distributor);
            for (Account account : accounts) {
                registration.registerApp(context, account.getUsername() + "@" + account.getInstance());
            }
            dialog.dismiss();
        });
        alert.show();
    }
}
