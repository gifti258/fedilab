/* Copyright 2018 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.asynctasks;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.lang.ref.WeakReference;

import app.fedilab.android.client.API;
import app.fedilab.android.client.APIResponse;
import app.fedilab.android.interfaces.OnRetrievePeertubeInterface;


/**
 * Created by Thomas on 16/10/2018.
 * Retrieves peertube single
 */

public class RetrievePeertubeSingleCommentsAsyncTask {


    private final String videoId;
    private final OnRetrievePeertubeInterface listener;
    private final WeakReference<Context> contextReference;
    private final String instanceName;
    private APIResponse apiResponse;


    public RetrievePeertubeSingleCommentsAsyncTask(Context context, String instanceName, String videoId, OnRetrievePeertubeInterface onRetrievePeertubeInterface) {
        this.contextReference = new WeakReference<>(context);
        this.videoId = videoId;
        this.listener = onRetrievePeertubeInterface;
        this.instanceName = instanceName;
        doInBackground();
    }

    protected void doInBackground() {
        new Thread(() -> {
            API api = new API(this.contextReference.get());
            apiResponse = api.getSinglePeertubeComments(this.instanceName, videoId);
            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> listener.onRetrievePeertubeComments(apiResponse);
            mainHandler.post(myRunnable);
        }).start();
    }

}
