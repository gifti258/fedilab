/* Copyright 2017 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.asynctasks;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Looper;

import java.lang.ref.WeakReference;

import app.fedilab.android.client.API;
import app.fedilab.android.client.APIResponse;
import app.fedilab.android.client.Entities.Account;
import app.fedilab.android.client.Entities.Emojis;
import app.fedilab.android.client.GNUAPI;
import app.fedilab.android.client.PeertubeAPI;
import app.fedilab.android.helper.Helper;
import app.fedilab.android.interfaces.OnUpdateAccountInfoInterface;
import app.fedilab.android.sqlite.AccountDAO;
import app.fedilab.android.sqlite.CustomEmojiDAO;
import app.fedilab.android.sqlite.Sqlite;

import static app.fedilab.android.activities.BaseMainActivity.social;

/**
 * Created by Thomas on 17/05/2017.
 * Manage the synchronization with the authenticated account and update the db not
 */

public class UpdateAccountInfoByIDAsyncTask {


    private final OnUpdateAccountInfoInterface listener;
    private final WeakReference<Context> contextReference;
    private final Account accountOld;

    public UpdateAccountInfoByIDAsyncTask(Context context, Account account, OnUpdateAccountInfoInterface onUpdateAccountInfoInterface) {
        this.contextReference = new WeakReference<>(context);
        this.listener = onUpdateAccountInfoInterface;
        this.accountOld = account;
        doInBackground();
    }


    protected void doInBackground() {
        new Thread(() -> {
            Account account = null;
            if (social == UpdateAccountInfoAsyncTask.SOCIAL.MASTODON || social == UpdateAccountInfoAsyncTask.SOCIAL.PLEROMA || social == UpdateAccountInfoAsyncTask.SOCIAL.PIXELFED) {
                account = new API(this.contextReference.get()).verifyCredentials();
                if (social == UpdateAccountInfoAsyncTask.SOCIAL.PIXELFED) {
                    account.setSocial("PIXELFED");
                }
            } else if (social == UpdateAccountInfoAsyncTask.SOCIAL.PEERTUBE) {
                account = new PeertubeAPI(this.contextReference.get()).verifyCredentials();
                account.setSocial("PEERTUBE");
            } else if (social == UpdateAccountInfoAsyncTask.SOCIAL.GNU || social == UpdateAccountInfoAsyncTask.SOCIAL.FRIENDICA) {
                account = new GNUAPI(this.contextReference.get()).verifyCredentials();
            }
            if (account == null)
                return;
            account.setInstance(Helper.getLiveInstance(contextReference.get()));
            SQLiteDatabase db = Sqlite.getInstance(this.contextReference.get().getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
            if (accountOld != null) {
                account.setInstance(accountOld.getInstance());
                account.setToken(accountOld.getToken());
                account.setRefresh_token(accountOld.getRefresh_token());
                account.setClient_id(accountOld.getClient_id());
                account.setClient_secret(accountOld.getClient_secret());
                new AccountDAO(this.contextReference.get(), db).updateAccountCredential(account);
            }
            if (social == UpdateAccountInfoAsyncTask.SOCIAL.PEERTUBE) {
                new API(contextReference.get()).refreshToken(account);
            }
            if (social == UpdateAccountInfoAsyncTask.SOCIAL.MASTODON || social == UpdateAccountInfoAsyncTask.SOCIAL.PLEROMA) {
                try {
                    APIResponse response = new API(contextReference.get()).getCustomEmoji();
                    if (response != null && response.getEmojis() != null && response.getEmojis().size() > 0) {
                        new CustomEmojiDAO(contextReference.get(), db).removeAll();
                        for (Emojis emojis : response.getEmojis()) {
                            if (emojis.isVisible_in_picker()) {
                                new CustomEmojiDAO(contextReference.get(), db).insertEmoji(emojis);
                            }
                        }
                    }
                } catch (Exception ignored) {
                }
            }
            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> listener.onUpdateAccountInfo(false);
            mainHandler.post(myRunnable);
        }).start();
    }
}
