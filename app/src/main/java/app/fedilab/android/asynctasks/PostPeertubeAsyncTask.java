/* Copyright 2019 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.asynctasks;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.lang.ref.WeakReference;

import app.fedilab.android.client.APIResponse;
import app.fedilab.android.client.Entities.Peertube;
import app.fedilab.android.client.PeertubeAPI;
import app.fedilab.android.interfaces.OnRetrievePeertubeInterface;


/**
 * Created by Thomas on 09/01/2019.
 * Update a Peertube video
 */

public class PostPeertubeAsyncTask {


    private final OnRetrievePeertubeInterface listener;
    private final WeakReference<Context> contextReference;
    private final Peertube peertube;
    private APIResponse apiResponse;


    public PostPeertubeAsyncTask(Context context, Peertube peertube, OnRetrievePeertubeInterface onRetrievePeertubeInterface) {
        this.contextReference = new WeakReference<>(context);
        this.listener = onRetrievePeertubeInterface;
        this.peertube = peertube;
        doInBackground();
    }


    protected void doInBackground() {
        new Thread(() -> {
            PeertubeAPI peertubeAPI = new PeertubeAPI(this.contextReference.get());
            apiResponse = peertubeAPI.updateVideo(peertube);
            if (apiResponse != null && apiResponse.getPeertubes() != null && apiResponse.getPeertubes().size() > 0)
                apiResponse.getPeertubes().get(0).setUpdate(true);
            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> listener.onRetrievePeertube(apiResponse);
            mainHandler.post(myRunnable);
        }).start();
    }
}
