/* Copyright 2017 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.asynctasks;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.lang.ref.WeakReference;

import app.fedilab.android.client.API;
import app.fedilab.android.client.APIResponse;
import app.fedilab.android.client.Entities.AccountCreation;
import app.fedilab.android.client.PeertubeAPI;
import app.fedilab.android.interfaces.OnPostStatusActionInterface;


/**
 * Created by Thomas on 15/06/2019.
 * Create a Mastodon account
 */

public class CreateMastodonAccountAsyncTask {

    private final OnPostStatusActionInterface listener;
    private final AccountCreation accountCreation;
    private final WeakReference<Context> contextReference;
    private final String instance;
    private final RetrieveInstanceRegAsyncTask.instanceType type;
    private APIResponse apiResponse;

    public CreateMastodonAccountAsyncTask(Context context, RetrieveInstanceRegAsyncTask.instanceType type, AccountCreation accountCreation, String instance, OnPostStatusActionInterface onPostStatusActionInterface) {
        this.contextReference = new WeakReference<>(context);
        this.listener = onPostStatusActionInterface;
        this.accountCreation = accountCreation;
        this.instance = instance;
        this.type = type;
        doInBackground();
    }

    protected void doInBackground() {
        new Thread(() -> {
            if (type == RetrieveInstanceRegAsyncTask.instanceType.MASTODON) {
                apiResponse = new API(contextReference.get(), instance, null).createAccount(accountCreation);
            } else {
                apiResponse = new PeertubeAPI(contextReference.get(), instance, null).createAccount(accountCreation);
            }
            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> listener.onPostStatusAction(apiResponse);
            mainHandler.post(myRunnable);
        }).start();
    }


}
